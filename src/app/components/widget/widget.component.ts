import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: '.dashboard-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.less']
})
export class WidgetComponent implements OnInit {
  @Input('widget') widget;

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
  }

  openCard() {
    this.widget.open = !this.widget.open;
  }
}
