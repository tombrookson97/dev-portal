import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from './header/header.module';
import { MainModule } from './navigation/main/main.module';
import { WidgetModule } from './widget/widget.module';


@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    MainModule,
    WidgetModule
  ],
  exports: [
    HeaderModule,
    MainModule,
    WidgetModule
  ]
})
export class ComponentsModule { }
