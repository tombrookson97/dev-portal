import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionsComponent } from './actions.component';
import { ClarityModule } from '@clr/angular';



@NgModule({
  declarations: [ActionsComponent],
  imports: [
    CommonModule,
    ClarityModule
  ],
  exports: [ActionsComponent]
})
export class ActionsModule { }
