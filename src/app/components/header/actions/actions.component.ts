import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/core/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'header-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.less']
})
export class ActionsComponent implements OnInit {
  userEmail: string = null;

  logoutConfirmModal: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
    ) {
      this.authService.authSubscribable.subscribe(auth => {
        this.userEmail = this.authService.authState.email
      });
  }

  ngOnInit() {
  }

  confirmLogout() {
    this.logoutConfirmModal = true;
  }

  logout() {
    this.authService.signOut();
    this.router.navigate(['/login']);
  }
}
