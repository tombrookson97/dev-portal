import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { ActionsModule } from './actions/actions.module';
import { SearchModule } from './search/search.module';
import { ClarityModule } from '@clr/angular';



@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    ActionsModule,
    SearchModule,
    ClarityModule
  ],
  exports: [HeaderComponent]
})
export class HeaderModule { }
