import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { ClarityModule } from '@clr/angular';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    ClarityModule,
    RouterModule
  ],
  exports: [MainComponent]
})
export class MainModule { }
