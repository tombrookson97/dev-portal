import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'navigation-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit {
  items: RouteItem[] = [
    {
      route: '/dashboard',
      label: '<clr-icon shape="home"></clr-icon>'
    },
    {
      route: '/shared-files',
      label: 'Shared Files'
    },
    {
      route: '/sso',
      label: 'SSO'
    },
    {
      route: '/service-statuses',
      label: 'Service Statuses'
    },
    {
      route: '/documentation',
      label: 'Documentation'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}

interface RouteItem {
  route: string,
  label: string
}