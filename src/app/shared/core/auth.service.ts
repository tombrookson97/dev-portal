import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from "@angular/router";
import { UserService, User } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState: any = null;
  user: any = null;

  // Returns true if user is logged in
  get authenticated(): boolean {
    return this.authState !== null;
  }

  // Returns current user
  get currentUser(): any {
    return this.authenticated ? this.authState.auth : null;
  }

  // Returns current user UID
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  get authSubscribable(): any {
    return this.af.user;
  }

  constructor(
    private af: AngularFireAuth,
    private router: Router,
    private us: UserService
  ) {
    af.user.subscribe(auth => {
      this.authState = auth;

      if (this.authState !== null) {
        console.log('uid: ', this.authState);
      }
    })
  }

  public signInWithEmailAndPassword(email: string, password: string) {
    return this.af.auth.signInWithEmailAndPassword(email, password);
  }

  public signOut() {
    this.af.auth.signOut().then(function() {
      console.log('done');
    }).catch(function(error) {
      // An error happened.
    });
  }
}
