import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service';

export interface User {
  firstName: string,
  lastName: string,
  email: string,
  joined_at: Date
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private af: AngularFirestore
  ) { }

  /* Get All User Docs */
  public getUserDocs() {
    return this.af.collection('users').snapshotChanges();
  }

  /* Get User Doc */
  public getUserDoc(id: string) {
    return this.af.collection('users').doc(id);
  }
  /* Create User Doc */
  public createUserDoc(id: string, data: User) {
    return new Promise<any>((resolve, reject) => {
      this.af.collection('users')
        .doc(id)
        .set(data)
        .then(res => {}, err => reject(err));
    });
  }
}
