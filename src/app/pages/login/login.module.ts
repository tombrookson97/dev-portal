import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { ClarityModule } from '@clr/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from 'src/app/shared/core/auth.service';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    ClarityModule,
    ReactiveFormsModule
  ],
  providers: [AuthService]
})
export class LoginModule { }
