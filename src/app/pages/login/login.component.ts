import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/shared/core/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  authErr: boolean = false;

  constructor(private authService: AuthService, 
    private router: Router) {
    this.loginForm = this.createFormGroup();
  }

  ngOnInit() {
  }

  createFormGroup() {
    return new FormGroup({
      email: new FormControl("test@test.com"),
      password: new FormControl("password123")
    });
  }

  onSubmit() {
    this.authErr = false;
    const results = this.loginForm.value;

    this.authService.signInWithEmailAndPassword(results.email, results.password).then(res => {
      this.router.navigate(['/']);
    }).catch(err => {
      this.authErr = true;
    });
  }
}
