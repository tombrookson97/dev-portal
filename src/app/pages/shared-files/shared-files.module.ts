import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedFilesComponent } from './shared-files.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { ClarityModule } from '@clr/angular';



@NgModule({
  declarations: [SharedFilesComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    ClarityModule
  ]
})
export class SharedFilesModule { }
