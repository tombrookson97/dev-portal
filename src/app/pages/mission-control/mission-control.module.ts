import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MissionControlComponent } from './mission-control.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { ClarityModule } from '@clr/angular';



@NgModule({
  declarations: [MissionControlComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    ClarityModule
  ],
  exports: [MissionControlComponent]
})
export class MissionControlModule { }
