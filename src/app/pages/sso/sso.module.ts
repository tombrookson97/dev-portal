import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SsoComponent } from './sso.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { ClarityModule } from '@clr/angular';



@NgModule({
  declarations: [SsoComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    ClarityModule
  ],
  exports: [SsoComponent]
})
export class SsoModule { }
