import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ClarityModule } from '@clr/angular';
import { ComponentsModule } from 'src/app/components/components.module';



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    ClarityModule
  ]
})
export class DashboardModule { }
