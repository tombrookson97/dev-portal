import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/core/user.service';
import { AuthService } from 'src/app/shared/core/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {
  userFirstName: string = '-';

  constructor(
    private userService: UserService,
    private authService: AuthService
  ) {
    this.getUserDetails();
  }

  ngOnInit() {
  }

  getUserDetails() {
    this.authService.authSubscribable.subscribe(auth => {
      this.userService.getUserDoc(this.authService.authState.uid).snapshotChanges().subscribe(user => {
        if (user.payload.exists) {
          const userDoc: any = user.payload.data();
          this.userFirstName = userDoc['firstName'];
        }
      });
    });
  }

  openCard(card) {
    console.log(card);
  }
}

interface Widget {
  title: string,
  text: string,
  open?: boolean
}