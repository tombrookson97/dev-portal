import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginModule } from './login/login.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { SharedFilesModule } from './shared-files/shared-files.module';
import { DocumentationModule } from './documentation/documentation.module';
import { SsoModule } from './sso/sso.module';
import { MissionControlModule } from './mission-control/mission-control.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LoginModule,
    DashboardModule,
    SharedFilesModule,
    DocumentationModule,
    SsoModule,
    MissionControlModule
  ]
})
export class PagesModule { }
