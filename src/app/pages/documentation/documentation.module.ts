import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentationComponent } from './documentation.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { ClarityModule } from '@clr/angular';



@NgModule({
  declarations: [DocumentationComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    ClarityModule
  ],
  exports: [DocumentationComponent]
})
export class DocumentationModule { }
