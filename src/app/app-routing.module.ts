import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { redirectUnauthorizedTo, canActivate } from '@angular/fire/auth-guard';
import { SharedFilesComponent } from './pages/shared-files/shared-files.component';
import { DocumentationComponent } from './pages/documentation/documentation.component';
import { SsoComponent } from './pages/sso/sso.component';
import { MissionControlComponent } from './pages/mission-control/mission-control.component';

const redirectUnauthorizedToLogin = redirectUnauthorizedTo(['login']);


const routes: Routes = [
  { path: '', component: DashboardComponent,
  ...canActivate(redirectUnauthorizedToLogin) },
  { path: 'shared-files', component: SharedFilesComponent,
  ...canActivate(redirectUnauthorizedToLogin) },
  { path: 'documentation', component: DocumentationComponent,
  ...canActivate(redirectUnauthorizedToLogin) },
  { path: 'sso', component: SsoComponent,
  ...canActivate(redirectUnauthorizedToLogin) },
  { path: 'mission-control', component: MissionControlComponent,
  ...canActivate(redirectUnauthorizedToLogin) },
  { path: 'login', component: LoginComponent },

  { path: '**', component: DashboardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
