// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAW8Cg3MPbuO4BBTWtw8JqHAhI61tnrj3A",
    authDomain: "dev-portal-fb643.firebaseapp.com",
    databaseURL: "https://dev-portal-fb643.firebaseio.com",
    projectId: "dev-portal-fb643",
    storageBucket: "dev-portal-fb643.appspot.com",
    messagingSenderId: "734920069845",
    appId: "1:734920069845:web:c5b100c5a9041bd143085c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
